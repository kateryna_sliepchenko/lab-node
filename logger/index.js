const fs = require('fs');
const path = require('path');

const loggerPath = path.join(__dirname, '/logs.txt');

// check logger file, if not exist then create it
if (!fs.existsSync(loggerPath)) {
    fs.writeFileSync(loggerPath, 'utf8');
}

// open stream for update our log
const stream = fs.createWriteStream(
    loggerPath,
    { flags: 'a' }
);

stream.on('error', (err) => {
    console.error(`Logger: ${err}`);
});

stream.on('finish', () => {
    console.log('Logger: done');
});

module.exports = {
    log: function (text) {
        const now = (new Date()).toISOString();
        
        console.log(`${now} - ${text}`);
        stream.write(`${now} - ${text}\n`);
    },
    
    error: function (error) {
        const now = (new Date()).toISOString();
    
        console.error(`${now} - Error: ${error}`);
        stream.write(`${now} - Error: ${error}\n`);
    }
};