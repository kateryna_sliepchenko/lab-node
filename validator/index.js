const Logger = require('./../logger');

module.exports = function(request, response) {
    const methods = {
        // Validation for file extension log, txt, json, yaml, xml, js files
        required: function(...names) {
            const missedParams = [];
            
            names.forEach(name => {
                if (request.body.hasOwnProperty(name) === false) {
                    missedParams.push(name);
                }
            });
    
            if (missedParams.length > 0) {
                Logger.error(`Params [${missedParams}] is required!`);
                response.status(422).send({'message': `Params [${missedParams}] is required!`});
            }
    
            return methods;
        },
        extension: function() {
            const fullname = request.body.filename || request.params.filename || '';
            const [, extension] = fullname.split('.');
    
            if (extension === undefined) {
                Logger.error(`File name ${fullname} is invalid, extension is required`);
                response.status(422).send({'message': `File name ${fullname} is invalid, extension is required`});
            }
    
            switch (extension) {
                case 'log':
                case 'txt':
                case 'json':
                case 'yaml':
                case 'xml':
                case 'js': {
                    // Logger.log(`File extension ${extension} is valid`);
                    break;
                }
        
                default: {
                    Logger.error(`File extension ${extension} is invalid`);
                    response.status(422).send({'message': `File extension ${extension} is invalid`});
                    break;
                }
            }
            
            return methods;
        }
    };
    
    return methods;
}