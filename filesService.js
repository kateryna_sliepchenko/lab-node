// requires...
const fs = require('fs').promises;
const path = require('path');

const Logger = require('./logger');
const Validator = require('./validator');

// constants...
const FILES_DIRECTORY = path.join(__dirname, '/files/');

// GET: /api/files
function getFiles(request, response) {
    Logger.log('Getting file list');
    
    return fs.readdir(FILES_DIRECTORY)
        .then(files => {
            Logger.log(`File list was returned. [${files}]`);
    
            response
                .status(200)
                .send({'message': 'Success', 'files': files});
        })
        .catch(unexpectedErrorHandler(request, response));
}

// POST: /api/files
function createFile(request, response) {
    Logger.log('Creating file');
    
    // TODO: Implement express validators
    Validator(request, response)
        .required('filename', 'content')
        .extension();

    const fileName = request.body.filename;
    const fileContent = request.body.content;
    
    Logger.log(`File name: ${fileName}`);
    Logger.log(`File content: ${fileContent}`);
    
    return fs.writeFile(FILES_DIRECTORY + fileName, fileContent, 'utf8')
        .then(() => {
            Logger.log(`File ${fileName} successfully created`);
        
            response
                .status(201)
                .send({'message': `File ${fileName} successfully created`});
        })
        .catch(unexpectedErrorHandler(request, response));
}

// GET: /api/files/:name
function getFile (request, response) {
    Logger.log('Getting file');
    
    Validator(request, response)
        .required('filename')
        .extension();
    
    const fileName = request.params.filename;
    const [, extension] = fileName.split('.');
    
    Logger.log(`File name: ${fileName}`);
    
    return Promise
        .all([
            fs.stat(FILES_DIRECTORY + fileName),
            fs.readFile(FILES_DIRECTORY + fileName, 'utf8')
        ])
        .then(([stats, content]) => {
            Logger.log(`File ${fileName} successfully found`);

            response
                .status(200)
                .send({
                    'message': `File ${fileName} successfully found`,
                    'filename': fileName,
                    'content': content,
                    'extension': extension,
                    'uploadedDate': stats.birthtime
                });
        })
        .catch((error) => {
            if (error.code === 'ENOENT') {
                // QUESTION: Why we are responding with 200 and not 201 status code?
                response
                    .status(404)
                    .send({'message': `File ${fileName} not found`});
            } else {
                unexpectedErrorHandler(request, response);
            }
        });
}

// PUT: /api/files/:name
const editFile = (request, response) => {
    Logger.log('Editing file');
    
    Validator(request, response)
        .required('content')
        .extension();
    
    const fileName = request.params.filename;
    const fileContent = request.body.content;
    
    Logger.log(`File name: ${fileName}`);
    Logger.log('Checking is file exist');
    
    return Promise
        .resolve()
        .then(() => fs.readFile(FILES_DIRECTORY + fileName, 'utf8'))
        .then(() => fs.writeFile(FILES_DIRECTORY + fileName, fileContent, 'utf8'))
        .then(() => {
            Logger.log(`File ${fileName} successfully updated`);
    
            response
                .status(200)
                .send({'message': `File ${fileName} successfully updated`});
        })
        .catch((error) => {
            if (error.code === 'ENOENT') {
                response
                    .status(404)
                    .send({'message': `File ${fileName} not found`});
            } else {
                unexpectedErrorHandler(request, response);
            }
        });
}

// DELETE: /api/files/:name
function deleteFile (request, response) {
    Logger.log('Deleting file');
    
    Validator(request, response)
        .required('filename')
        .extension();
    
    const fileName = request.params.filename;
    Logger.log(`File name: ${fileName}`);
    
    return fs.unlink(FILES_DIRECTORY + fileName)
        .then(() => {
            Logger.log(`File ${fileName} successfully deleted`);
            
            response
                .status(200)
                .send({'message': `File ${fileName} successfully deleted`});
        })
        .catch((error) => {
            if (error.code === 'ENOENT') {
                response
                    .status(404)
                    .send({'message': `File ${fileName} not found`});
            } else {
                unexpectedErrorHandler(request, response);
            }
        });
}

function unexpectedErrorHandler (request, response) {
    return (error) => {
        Logger.error(`Unexpected error! ${error}`);
    
        response
            .status(500)
            .send({'message': `Unexpected error! ${error}`});
    };
}

module.exports = {
    getFiles,
    createFile,
    getFile,
    editFile,
    deleteFile
}
